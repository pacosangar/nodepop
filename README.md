# nodepop API

## Iniciar la base de datos
---
~~~
npm run installdb
~~~
- Al ejecutar se borrar la base de datos y se insertaran los datos que se encuentran en el fichero data.json, que esta guadado en la carpeta json-data.

## Iniciar mi aplicación.
---
~~~
npm run dev
~~~
- Iniciamos la aplicación, con la conexión a la base de datos.

## Iniciar microservicio creación de thumbnail
---
~~~
npm run thumbnailService
~~~
- Iniciamos el microservicio de creación de thumbnails
  
## Página de inicio de mi website
---

http://localhost:3000

---

# Página principal de la API

Para poder visualizar la página el usuario tiene que estar logado, en caso contrario no puede visualizar esta página.

![Listado anuncios](./public/images/readme/anincios_API_AUTH.png);

// GET /api/ads 

http://localhos:3000/api/ads

Muestra el listado completo de registros que tiene la base de datos, en formato json.



    {
        "tags": [
            "kids"
        ],
        "_id": "601b2e83ec203920a013f1cc",
        "name": "Vigila bebes chicco",
        "sale": true,
        "price": 15,
        "photo": "bebe-guard.webp",
        "__v": 0
    },
    {
        "tags": [
            "lifestyle",
            "sport"
        ],
        "_id": "601b2e83ec203920a013f1cd",
        "name": "Bicicleta BH 27",
        "sale": true,
        "price": 100,
        "photo": "bici-bh.webp",
        "__v": 0
    },
    {
        "tags": [
            "lifestyle",
            "sport",
            "kids"
        ],
        "_id": "601b2e83ec203920a013f1ce",
        "name": "Bicicleta BH niños",
        "sale": true,
        "price": 80,
        "photo": "bici-bh-kids.webp",
        "__v": 0
    },
    {
        "tags": [
            "lifestyle",
            "sport"
        ],
        "_id": "601b2e83ec203920a013f1cf",
        "name": "Bicicleta Btwin 27 Adulto",
        "sale": true,
        "price": 89,
        "photo": "bici-btwin.webp",
        "__v": 0
    },
    ...

---
# Autenticación en el API

http://localhost:3000/api/loginJWT

![login](./public/images/readme/loginAPI.png)

---
# Uso de filtros en el API

## Paginación limit skip (?limit - ?skip)
---
Número de elementos que se mostraran 

skip elemento que se 'salta', para la nueva pagina.


http://localhost:3000/api/ads?limit=2


    {
        "tags": [
            "kids"
        ],
        "_id": "601b2e83ec203920a013f1cc",
        "name": "Vigila bebes chicco",
        "sale": true,
        "price": 15,
        "photo": "bebe-guard.webp",
        "__v": 0
    },
    {
        "tags": [
            "lifestyle",
            "sport"
        ],
        "_id": "601b2e83ec203920a013f1cd",
        "name": "Bicicleta BH 27",
        "sale": true,
        "price": 100,
        "photo": "bici-bh.webp",
        "__v": 0
    }


http://localhost:3000/api/ads?limit=2&skip=2

    {
        "tags": [
            "lifestyle",
            "sport",
            "kids"
        ],
        "_id": "601b2e83ec203920a013f1ce",
        "name": "Bicicleta BH niños",
        "sale": true,
        "price": 80,
        "photo": "bici-bh-kids.webp",
        "__v": 0
    },
    {
        "tags": [
            "lifestyle",
            "sport"
        ],
        "_id": "601b2e83ec203920a013f1cf",
        "name": "Bicicleta Btwin 27 Adulto",
        "sale": true,
        "price": 89,
        "photo": "bici-btwin.webp",
        "__v": 0
    }

## Filtrar por tag (?tag)
---
Filtra por el tag concreto que le indiquemos.


http://localhost:3000/api/ads?tag=books

    {
        "tags": [
            "books"
        ],
        "_id": "601b2e83ec203920a013f1d3",
        "name": "Stephen King Pesadillas y Alucinaciones I",
        "sale": true,
        "price": 15,
        "photo": "book-02.webp",
        "__v": 0
    },
    {
        "tags": [
            "books"
        ],
        "_id": "601b2e83ec203920a013f1d4",
        "name": "Stephen King Pesadillas y Alucinaciones II",
        "sale": true,
        "price": 15,
        "photo": "book-01.webp",
        "__v": 0
    }

## Filtrar por tipo anuncio venta o busqueda (?sale=true -> venta, ?sale=false -> busqueda)
---

http://localhost:3000/api/ads?sale=true
http://localhost:3000/api/ads?sale=false

    {
        "tags": [
            "lifestyle",
            "sport",
            "kids"
        ],
        "_id": "601d96c532e6ad36d83cc64d",
        "name": "Bicicleta BH niños",
        "sale": false,
        "price": 80,
        "photo": "bici-bh-kids.webp",
        "__v": 0
    },
    {
        "tags": [
            "lifestyle"
        ],
        "_id": "601d96c532e6ad36d83cc651",
        "name": "Bolso Channel",
        "sale": false,
        "price": 97,
        "photo": "bolso-pijo.webp",
        "__v": 0
    }

## Filtrar por nombre (?name)
---
Filtramos el nombre del artículo anunciado (que empiede por)

http://localhost:3000/api/ads?name=vi

    {
        "tags": [
            "kids"
        ],
        "_id": "601d96c532e6ad36d83cc64b",
        "name": "Vigila bebes chicco",
        "sale": true,
        "price": 20,
        "photo": "bebe-guard.webp",
        "__v": 0
    }

## Filtro ordenar por... (?sort)

Ordena por el campo que le indiquemos ascendente o descendentemente

Ascendente 
http://localhost:3000/api/ads?sort=price
Descendemnte
http://localhost:3000/api/ads?sort=price-



## Filtrar por un rango de precios (?price)
---
Filtraremos por el precio, permitiendo las siguientes combinaciones:

- 16-50 buscará anuncios con precio incluido entre estos valores
  - http://localhost:3000/api/ads?price=16-50 

- 7000- buscará los que tengan precio mayor que 7000 
    - http://localhost:3000/api/ads?price=7000-

- -18 buscará los que tengan precio menor de 18
  - http://localhost:3000/api/ads?price=-18 
  
- 50 buscará los que tengan precio igual a 50
  - http://localhost:3000/api/ads?price=80

~~~
    {
        "tags": [
            "lifestyle",
            "sport",
            "kids"
        ],
        "_id": "601d96c532e6ad36d83cc64d",
        "name": "Bicicleta BH niños",
        "sale": false,
        "price": 80,
        "photo": "bici-bh-kids.webp",
        "__v": 0
    }
~~~

## Recuperar solo un o unos campos concretos (?fileds)
---
### Un campo.
http://localhost:3000/api/ads?fields=tags

~~~

{
    "tags": [
        "motor"
    ],
    "_id": "601d96c532e6ad36d83cc64a"
},
{
    "tags": [
        "kids"
    ],
    "_id": "601d96c532e6ad36d83cc64b"
},
...
~~~
### Varios campos concretos.

http://localhost:3000/api/ads?fields=tags+name
~~~
{

    "tags": [
        "motor"
    ],
    "_id": "601d96c532e6ad36d83cc64a",
    "name": "Audi A6"

},
{

    "tags": [
        "kids"
    ],
    "_id": "601d96c532e6ad36d83cc64b",
    "name": "Vigila bebes chicco"

},
...
~~~
---
# Lista de tags existentes

//GET /api/ads/tags

http://localhost:3000/api/ads/tags
~~~
{
    "result": [
        "books",
        "kids",
        "lifestyle",
        "motor",
        "sport"
    ]
}
~~~
---
# Obtener un anuncio por su id

//GET /api/ads:id

Buscamos en la base de datos un rregistro con un id concreto.

http://localhost:3000/api/ads/601d96c532e6ad36d83cc64c
~~~
result": {

    "tags": [
        "lifestyle",
        "sport"
    ],
    "_id": "601d96c532e6ad36d83cc64c",
    "name": "Bicicleta BH 27",
    "sale": true,
    "price": 100,
    "photo": "bici-bh.webp",
    "__v": 0
}
~~~
---
# DML(Data Manipulation Language)

INSERT - Creación de un anuncio.

// POST /api/ads (body)

![Insert](./public/images/readme/postman-insercion-datos.png)

UPDATE -Actualización / modificación de un anuncio.

![Before Update](public/images/readme/postman-before-update.png)

![Update](public/images/readme/postman-data-update.png)

DELETE - Borra un anuncio

//DELETE /api/ads:id

![Before Delete](public/images/readme/postman-before-delete.png)

![Delete](public/images/readme/postman-data-delete.png)

![After Delete](public/images/readme/postman-after-delete.png)

# Uso de filtros en el webSite

## Paginación limit skip (?limit - ?skip)
---
Número de elementos que se mostraran 

skip elemento que se 'salta', para la nueva pagina.


http://localhost:3000/?limit=2

http://localhost:3000/?limit=2&skip=2

   
## Filtrar por tag (?tag)
---
Filtra por el tag concreto que le indiquemos.


http://localhost:3000/?tag=books

    

## Filtrar por tipo anuncio venta o busqueda (?sale=true -> venta, ?sale=false -> busqueda)
---

http://localhost:3000/?sale=true

http://localhost:3000/?sale=false


## Filtrar por nombre (?name)
---
Filtramos el nombre del artículo anunciado (que empiede por)

http://localhost:3000/?name=vi


## Filtro ordenar por... (?sort)
___
Ordena por el campo que le indiquemos ascendente o descendentemente

Ascendente 
http://localhost:3000/?sort=price

Descendemnte
http://localhost:3000/?sort=price-



## Filtrar por un rango de precios (?price)
---
Filtraremos por el precio, permitiendo las siguientes combinaciones:

- 16-50 buscará anuncios con precio incluido entre estos valores
  - http://localhost:3000/?price=16-50 

- 7000- buscará los que tengan precio mayor que 7000 
    - http://localhost:3000/?price=7000-

- -18 buscará los que tengan precio menor de 18
  - http://localhost:3000/?price=-18 
  
- 50 buscará los que tengan precio igual a 50
  - http://localhost:3000/?price=80


## Recuperar solo un o unos campos concretos (?fileds)
---
### Un campo.
http://localhost:3000/?fields=tags


### Varios campos concretos.

http://localhost:3000/?fields=tags+name

---
# Lista de tags existentes

Los tags existentes se muestran en la página index.html en la parte superior.

http://localhost:3000

---

# Atenticación en el API

Para acceder usaremos las credenciales sugeridas en el enunciado de la practica.

http://localhost:3000/api/loginJWT

---

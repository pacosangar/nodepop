'use strict'
//Este es el modelo.


//Defino el esquema

const mongoose = require('mongoose');

const adSchema = mongoose.Schema({
    name: {
        type: String,
        index: true
    },
    sale: {
        type: Boolean,
        index: true
    },
    price: {
        type: Number,
        index: true
    },
    photo: String,
    tags: {
        type: [String],
        index: true
    },
});


//En los métodos de mongoose que nosotros creemos no usar Arroy functions
//Esto lo haremos antes de crear el modelo

adSchema.statics.lista = function (filtro, limit, skip, fields, sort) {
    const query = Ad.find(filtro);
    query.limit(limit); //Paginación
    query.skip(skip); //Para 'saltarnos' un numero de elementos
    query.select(fields);//Muestro un campo o campos concreto
    query.sort(sort); //Ordeno por ..
    return query.exec(); //Para devolver una promesa
}

//Creo el modelo con el esquema definido

const Ad = mongoose.model('Ad', adSchema);


//Exporto el modelo (Esto es opcional pero es recomendable hacerlo)

module.exports = Ad;
'use strict'

const mongoose = require('mongoose');

//                    cadena de conexión URI       opciones
//Nos conectamos a nuestra BBDD
mongoose.connection.on('error', err=>{
    console.log('Error de conexión', err);
    process.exit(1); //Tiramos la aplicación, si base de datos mi API no tiene sentido.
});

mongoose.connection.once('open', ()=>{ //La primera vez que se conecte.
    console.log('Conectado a MongoDB en', mongoose.connection.name);
    
});

//                 cadena de conexión URI       
mongoose.connect(process.env.MONGODB_CONNECTION_STR, { //opciones
    useNewUrlParser: true, 
    useUnifiedTopology: true
}); //Nos conectamos a nuestra BBDD

module.exports = mongoose.connection;

module.exports = {
    connectMongouse: require('./connectMongoose'),
    Ad: require ('./Ad'),
    User: require ('./User'),
    mongoose: require('mongoose')
};
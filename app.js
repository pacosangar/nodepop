var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const session = require('express-session');
const sessionAuth = require('./lib/sessionAuthMiddleware');

const loginController = require('./controllers/loginController.js');

const MongoStore = require('connect-mongo');
var app = express();

//Conexión con la base de datos
require('./models/connectMongoose');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').__express);

//Cualquier vista tendra acceso a esta variable global
app.locals.title = 'NodePop';

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//Middleware de ficheros estáticos.
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Rutas del API
 */
app.post('/api/loginJWT', loginController.postJWT);
app.use('/api/ads',       require('./routes/api/ads'));


// Setup de i18n
const i18n = require('./lib/i18nConfigure');
app.use(i18n.init);

/**
 * Middleware de Gestion de sesiones del website
 */
 app.use(session({
  name: 'nodeapi-session',
  secret: 'Pirata$69aLfaD3lt4##',
  saveUninitialized: true,
  resave: false,
  cookie: {
    secure: process.env.NODE_ENV !== 'development', // solo se envian al servidor cuando la petición es HTTP
    maxAge: 1000 * 60 * 60 * 24 * 3 // 3 días de inactividad
  },
  store: MongoStore.create({ mongoUrl: process.env.MONGODB_CONNECTION_STR})
}));

// hacemos disponible la sesion en todas las vistas
app.use((req, res, next) => {
  res.locals.session = req.session;
  next();
});




/**
 * Rutas de mi Website
 */

app.use('/',                           require('./routes/index'));
app.use('/change-locale',              require('./routes/change-locale'));
app.get('/auth/login',                 loginController.index);
app.post('/auth/login',                loginController.post);
app.get('/auth/logout',                loginController.logout);
app.get('/zonavip/newAd',              sessionAuth, require('./controllers/privadoController').index);
app.use('/users',                      require('./routes/users'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {

  // es un error de validación?
  if (err.array) {
    const errorInfo = err.array({ onlyFirstError: true })[0];
    err.message = `Not valid - ${errorInfo.param} ${errorInfo.msg}`;
    err.status = 422;
  }

  res.status(err.status || 500);

  if (isAPIRequest(req)) {
    res.json({ error: err.message });
    return;
  }

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.render('error');
});

function isAPIRequest(req) {
  return req.originalUrl.indexOf('/api/') === 0;
}

module.exports = app;

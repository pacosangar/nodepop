"use strict";

const jwt = require('jsonwebtoken');
const { User } = require("../models");

class LoginController {
  /**
   * GET /login
   */
  index(req, res, next) {
    res.locals.email = "";
    res.locals.error = "";
    res.render("login");
  }

   /**
   * POST /login
   */

  async post(req, res, next) {
    try {
      const { email, password } = req.body;
      //Buscar el usurio en la BBDD
      const user = await User.findOne({ email });
      
      //Si no se encuentra el usuario --> error
      //Si no coincide la contraseña --> error
      if (!user || !(await user.comparePassword (password))) {
        res.locals.email = email;
        res.locals.error = "Credenciales invalidas";
        res.render("login");
        return;
      }
      /**
       * Si el usuario existe y la clave es correcta
       */

      // Guardar en la sesion del usuario su _id
      req.session.authUser = {
        _id: user._id
      };

      //redirigir a zona privada

      res.redirect('/zonavip/newAd');

    } catch (err) {
      next(err);
    }
  }

  /**
   * POST /loginJWT
   */
   async postJWT(req, res, next) {
    try {
      const { email, password } = req.body;

      //Buscar el usurio en la BBDD
      const user = await User.findOne({ email });
      
      //Si no se encuentra el usuario --> error
      //Si no coincide la contraseña --> error
      if (!user || !(await user.comparePassword (password))) {
        const error = new Error('Invalid credentials');
        error.status = 401;
        next(error);
        return;
      }
      /**
       * Si el usuario existe y la clave es correcta
       * Creamos un token JWT (firmado).
       */

       jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: '2h' }, (err, jwtToken) => {
        if (err) {
          next(err);
          return;
        }
        // devolveselo al cliente
        res.json({ token: jwtToken });
      });

    } catch (err) {
      next(err);
    }
  }




  /**
   * GET /logout
   */

  logout (req, res, next){
    //Borra la session de la memoria y crea una vacia
    req.session.regenerate(err =>{
      if(err){
        next(err);
        return;
      }
      res.redirect('/');
    }); 
  }
}

module.exports = new LoginController();

var express = require('express');
var router = express.Router();

const Ad = require("../models/Ad");
const { filterByPrice } = require("../lib/utils/filterByPrice");

/* GET home page. */
//Recuperamos todos los elementos de la base de datos
//http://localhost:3000/?name=Calas
router.get('/', async function (req, res, next) {
  try {
    //Filtro por name
    const name = req.query.name;
    const filter = {};
    if (name) {
      //filter.name = name
      filter.name = new RegExp('^'+ req.query.name, "i");
    }
    //Filtro por sale
    const sale = req.query.sale;
    if (sale) {
      filter.sale = sale
    }
    //Filtro por tag
    const tag = req.query.tag
    if (tag) {
      filter.tags = tag;
    }
    //Filtro por precio
    const price = req.query.price;
    if (price) {
      filter.price = filterByPrice(price);
    }
    //Paginación
    const limit = parseInt(req.query.limit);
    //para que empiece por el 3 por la segunda pagina * Ejmplo
    const skip = parseInt(req.query.skip);
    //Recuperar solo un o unos campos concretos
    const fields = req.query.fields;
    //Ordenación
    const sort = req.query.sort;
    //http://localhost:3000/?sort=price%20-name
    //Ordena por precio ascendente y nombre descendente

    //                   Ad.find();   Lo mismo que la linea de abajo lo que pasa que me he 
    //                                     creado un metodo que lo hace
    const result = await Ad.lista(filter, limit, skip, fields, sort); //Lista es un metodo creado por mi en el modelo    
    //Obtener todos las tags existentes sin que se repitan
    const resultTags = await Ad.distinct("tags");
    if (!resultTags) {
      res.status(404).json({ error: 'not found' });
      return;
    }
    res.render('index', { resultSet: result, resultTags });
  } catch (error) {
    next(error);
  }
});

/*GET hompage */

router.get('/:id', async (req, res, next) => {
  try {
    const _id = req.params.id;
    const resultSet = await Ad.find({ _id });

    if (resultSet.length === 0) {
      next();

      return;
    }

    res.render('index', { resultSet });
  } catch (error) {
    next(error);
  }
});


module.exports = router;

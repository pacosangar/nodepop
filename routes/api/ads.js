//Controlador de la API

var express = require('express');
var router = express.Router();

//Cargo el modelo para utilizarlo
const Ad = require('../../models/Ad');

const jwtAuth = require('../../lib/jwtAuth');
//const fileUpload = require('../../lib/fileUpload');

const multer = require('multer');

const cote = require('cote');

const storage = multer.diskStorage({
  destination: process.env.UPLOAD_DESTINATION, //Directorio donde se va a guarda el fichero
  filename: function(req, file, cb){
      cb("", Date.now() + file.originalname) //nombre con el que guardamos el fichero
  }
});

var upload = multer({storage: storage});

const requester = new cote.Requester({name: 'client create thumbnail'});


const { filterByPrice } = require("../../lib/utils/filterByPrice");

//GET /api/ads/tags
//Obtener todos las tags existentes sin que se repitan
//http://localhost:3000/api/ads/tags
router.get('/tags', jwtAuth, async (req, res, next) =>{
  try {
    const resultTags = await Ad.distinct("tags");
    if (!resultTags) {
      res.status(404).json({ error: 'not found' });
      return;
    }
    res.json({ result: resultTags });

  } catch (error) {
    next(error);
  }
});

/* GET /api/ads */
//Recuperamos todos los elementos de la base de datos
//http://localhost:3000/api/ads?name=Calas

router.get('/', jwtAuth, async function (req, res, next) {
  try {
    //Filtro por name
    const name = req.query.name;
    const filter = {};
    if (name) {
      //filter.name = name
      filter.name = new RegExp('^'+ req.query.name, "i");
    }
    //Filtro por sale
    const sale = req.query.sale;
    if(sale){
      filter.sale = sale
    }
    //Filtro por tag
    const tag = req.query.tag
    if (tag) {
      filter.tags = tag;
    }
     //Filtro por precio
     const price = req.query.price;
     if (price) {
       filter.price = filterByPrice(price);
     }

    //Paginación
    const limit = parseInt(req.query.limit);
    //para que empiece por el 3 por la segunda pagina * Ejmplo
    const skip = parseInt(req.query.skip);
    //Recuperar solo un o unos campos concretos
    const fields =req.query.fields;
    //Ordenación
    const sort = req.query.sort;

    //                   Ad.find();   Lo mismo que la linea de abajo lo que pasa que me he 
    //                                     creado un metodo que lo hace
    const result = await Ad.lista(filter, limit, skip, fields, sort); //Lista es un metodo creado por mi en el modelo
    res.json(result);
  } catch (error) {
    next(error);
  }
});

//GET /api/ads:id
//Obtener UN anuncio
router.get('/:id', jwtAuth, async (req, res, next) => {
  try {
    const _id = req.params.id;
    const resultAd = await Ad.findOne({ _id: _id});
    if (!resultAd) {
      res.status(404).json({ error: 'not found' });
      return;
    }
    res.json({ result: resultAd });
  } catch (error) {
    next(error);
  }
});

// POST /api/ads (body)
// Crear un anuncio
//
router.post('/',upload.single('photo'), async (req, res, next) => {
  try {

    const file = req.file;
    const adData = req.body;

    let photo = {
      photo: process.env.DEFAULT_IMAGE
    }
    if(file){
      photo = {
        photo: file.filename
      }
      //createThumbnail(file.filename);
      //Llamamos al microservicio 
      requester.send({
        type: 'create thumbnail',
        imageName: file.filename,
        path: process.env.UPLOAD_DESTINATION

      }, resultado =>{
        console.log('Thumbnail creado');
      });

    }

    const adData2 = {...adData, ... photo};
    const ad = new Ad(adData2);
    const adCreated = await ad.save();

    res.status(201).json({ result: adCreated });

  } catch (error) {
    next(error);
  }
});


//PUT /api/ads:id (body)
//Modifica un anuncio
router.put('/:id', jwtAuth, async (req, res, next) => {
  try {
    const _id = req.params.id;
    const adData = req.body;

    const adUpdate = await Ad.findByIdAndUpdate({ _id }, adData, {
      new: true, //Para que se devuelva el anuncio actualizado
      useFindAndModify: false
    });

    if (!adUpdate) {
      res.status(404).json({ error: 'not found' });
      return;
    }

    res.json({ result: adUpdate })

  } catch (error) {
    next(error);
  }
});

//DELETE /api/ads:id
//Elimina un anuncio

router.delete('/:id', jwtAuth, async (req, res, next) => {
  try {
    const _id = req.params.id;

    await Ad.deleteOne({ _id });

    res.json();

  } catch (error) {
    next();
  }
});

module.exports = router;
"use strict";

require("dotenv").config();

//Conexión con la base de datos
//require('./connectMongoose');

//Cargo el modelo para utilizarlo
//const Ad = require('../models/Ad');

const { mongoose, connectMongoose, User, Ad } = require("../models");

//El fichero con los datos que queremos guardar en la base de datos
const data = require("../json-data/data.json");

const dataUser = require("../json-data/dataUser.json");

main().catch((err) => console.error(err));

async function main() {
  //Inicializamos colección de usuarios
  await initUsers();
  //Inicializamod colección de anuncios.
  await initAds();
  //Cerramos la conexión con la base de datos
  mongoose.connection.close();
}

//Eliminamos todos los datos de la base de datos,
async function initAds() {
  try {
    //Borramos todos los datos que contiene la base de datos
    const deleteData = await Ad.deleteMany();

    console.log(`Eliminados ${deleteData.deletedCount} anuncios`);
    console.log("Delete Ads Ok!!!");

    if (deleteData.ok === 1) {
      //Insertamos los datos en la base de datos ads
      const saveData = await Ad.insertMany(data);

      console.log(
        `Insertado${saveData.length > 1 ? "s" : ""} ${saveData.length} anuncio${
          saveData.length > 1 ? "s" : ""
        } `
      );
      console.log("Insert Ads OK !!!", saveData);
    }
  } catch (error) {
    console.log("Function initAds error", error);
  }
}

//Inserción del objeto con los usuarios

async function initUsers() {
  try {
    const deleteUser = await User.deleteMany();

    console.log(`Eliminados ${deleteUser.deletedCount} usuarios`);
    console.log("DeleteUser OK !!");
    if (deleteUser.ok === 1) {
      const saveUser = await User.insertMany([
        {
          email: "user@example.com",
          password: await User.hashPassword('1234')
        },
        {
          email: "fco.sanchezgarcia@gmail.com",
          password: await User.hashPassword('1234')
        },
      ]);

      console.log(
        `Insertado${saveUser.length > 1 ? "s" : ""} ${saveUser.length} usuario${
          saveUser.length > 1 ? "s" : ""
        } `
      );
      console.log("Insert Users OK !!!", saveUser);
    }
  } catch (error) {
    console.log("Función initUsers error", error);
  }
}

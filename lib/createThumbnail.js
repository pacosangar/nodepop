'use strict';

const jimp = require('jimp');

module.exports = async(imageIN,) => {
    //Leo la imagen
    const imgURL = process.env.UPLOAD_DESTINATION + '/' + imageIN;

    const image = await jimp.read(imgURL);

    image.resize(250,jimp.AUTO);

    const thumbURL = process.env.UPLOAD_DESTINATION + '/thumbnails/' + imageIN;

    //Guardamos la imagen
    image.write(thumbURL);
}
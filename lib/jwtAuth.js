"use strict";

const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  //Recojo el jwtTocken de la cabecera (puede ser de otros sitios)
  //viene en la cabecera              en un querystring, en el body
  const jwtToken =
    req.get("Authorization") || req.query.token || req.body.token;

  //comprobar que tenemos el token
  if (!jwtToken) {
    const error = new Error("No token provided");
    error.status = 401;
    next(error);
    return;
  }
  //Comprobamos que el token sea valido
  jwt.verify(jwtToken, process.env.JWT_SECRET, (err, payload) => {
    if (err) {
      err.status = 401;
      next(new Error('Invalid token provided'));
      return;
    }
    req.apiAuthUserId = payload._id;
    next();
  });
};

'use strict';

// modulo que exporta un middleware

module.exports = (req, res, next) => {
  if (!req.session.authUser) {
    res.redirect('/auth/login');
    return;
  }
  next();
};



'use strict';

//Servicio de creación de thumbnails.

const cote = require('cote');

const jimp = require('jimp');

//Declaramos el microservico
const responder = new cote.Responder({name: 'create thumbnail'});

const creaThumbnail = async(pathiIN, imageIN) => {
    //Leo la imagen
    const imgURL = pathiIN + '/' + imageIN;

    const image = await jimp.read(imgURL);

    image.resize(100,100);

    const thumbURL = pathiIN + '/thumbnails/' + imageIN;

    //Guardamos la imagen
    image.write(thumbURL);
};

//Logica del microservicio
responder.on('create thumbnail', (req, done) =>{
    const {path, imageName} = req;
    console.log('Service: ', imageName);

    //Llamamos a la funcion que crea el thumbnail de la imagen que hemos indicado
    creaThumbnail(path, imageName);

    //responder
    done('ok, thumbnail Create!!');

});


